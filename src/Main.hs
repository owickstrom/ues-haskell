module Main where 
  import Text.Printf(printf)
  import UES.Core
  import UES.Infrastructure
  import UES.Sample.RPS.Move
  import UES.Sample.RPS.Game
  import UES.Sample.RPS.Commands
  
  -- | Delegates a list of commands to an aggregate.
  handleAll :: Handler s c e -> Listener s e -> [e] -> [c] -> Snapshot s e
  handleAll cmdHandler listener events commands = 
    let handler = handle cmdHandler listener
    in foldl handler (events, Nothing) commands 
      
  -- Given no events, when these three commands are handled,
  -- "Nisse" should win as Paper beats Rock.
  
  main :: IO ()
  main = let 
      commands = [CreateGameCommand "1" "Kalle"
                ,MakeMoveCommand "1" "Kalle" Rock
                ,MakeMoveCommand "1" "Nisse" Paper]  
      (events, state) = handleAll handleCommand applyEvent [] commands
    in 
      case state of
        (Just s) -> printf "After %d events: %s\n" (length events) $ show s
        Nothing -> printf "Nothing happened..."
