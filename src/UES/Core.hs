module UES.Core where
  class (Eq c, Show c) => Command c where
      aggregateId :: c -> String
      
  type Handler s c e = (Maybe s -> c -> [e])
  type Listener s e = (Maybe s -> e -> Maybe s)
  type Snapshot s e = ([e], Maybe s)
