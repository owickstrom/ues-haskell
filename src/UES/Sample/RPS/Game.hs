module UES.Sample.RPS.Game where
  import Text.Printf(printf)
  import UES.Core
  import UES.Sample.RPS.PlayerId
  import UES.Sample.RPS.Move
  import UES.Sample.RPS.Commands
  import UES.Sample.RPS.Events

  type PlayerMove = (PlayerId, Move)

  data GameState = 
    CreatedGameState (Maybe (PlayerId, Move)) 
    | WonGameState PlayerId PlayerId
    | TiedGameState PlayerId PlayerId
    deriving (Eq)
    
  instance Show GameState where
    show (CreatedGameState Nothing) = "Game with no move"
    show (CreatedGameState (Just (p, _))) = printf "Game with move made by %s" p
    show (WonGameState w l) = printf "Game won by %s against %s" w l
    show (TiedGameState p1 p2) = printf "Tied game between %s and %s" p1 p2
    
  -- | Command handlers for Game
  handleCommand :: Handler GameState GameCommand GameEvent
  
  handleCommand Nothing (CreateGameCommand _ c) = [GameCreatedEvent c]
  
  handleCommand (Just (CreatedGameState Nothing)) (MakeMoveCommand _ p m) =
    [MoveMadeEvent p m]
      
  handleCommand (Just (CreatedGameState (Just (p1, m1)))) (MakeMoveCommand _ p2 m2)
    | m1 `beats` m2     = [moveEv, GameWonEvent p1 p2] -- p1 won
    | m2 `beats` m1     = [moveEv, GameWonEvent p2 p1] -- p2 won
    | otherwise         = [moveEv, GameTiedEvent p1 p2] -- tied     
    where moveEv = MoveMadeEvent p2 m2
    
  handleCommand _ _ = []
  
  -- | Event listeners for Game
  applyEvent :: Listener GameState GameEvent
  
  -- | When the game got created.
  applyEvent Nothing (GameCreatedEvent _) = 
    Just (CreatedGameState Nothing)

  -- | When the game was created and a player made a first move.
  applyEvent (Just (CreatedGameState Nothing)) (MoveMadeEvent p m) = 
    Just (CreatedGameState (Just (p, m)))

  -- | When the game was won.
  applyEvent (Just (CreatedGameState _)) (GameWonEvent winner loser) =
    Just (WonGameState winner loser)
      
  -- | When the game was tied.
  applyEvent (Just (CreatedGameState _)) (GameTiedEvent p1 p2) =
    Just (TiedGameState p1 p2)
          
  -- | Otherwise no change...
  applyEvent s _ = s

