module UES.Sample.RPS.Move where
  data Move = Rock | Paper | Scissors deriving (Eq, Show)
  
  beats :: Move -> Move -> Bool
  Rock        `beats` Scissors    = True
  Paper       `beats` Rock        = True
  Scissors    `beats` Paper       = True
  _           `beats` _           = False
