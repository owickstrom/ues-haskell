module UES.Sample.RPS.Commands where
  import UES.Core
  import UES.Sample.RPS.GameId
  import UES.Sample.RPS.PlayerId
  import UES.Sample.RPS.Move
  
  data GameCommand =
    CreateGameCommand GameId PlayerId
    | MakeMoveCommand GameId PlayerId Move 
    deriving (Eq, Show)

  instance Command GameCommand where
    aggregateId (CreateGameCommand gameId _) = gameId
    aggregateId (MakeMoveCommand gameId _ _) = gameId
