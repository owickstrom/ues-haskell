module UES.Sample.RPS.Events where
  import UES.Sample.RPS.PlayerId
  import UES.Sample.RPS.Move

  data GameEvent =
    GameCreatedEvent PlayerId
    | MoveMadeEvent PlayerId Move 
    | GameWonEvent PlayerId PlayerId
    | GameTiedEvent PlayerId PlayerId
    deriving (Eq, Show)
