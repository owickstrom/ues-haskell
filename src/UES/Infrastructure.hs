module UES.Infrastructure where
  import UES.Core
  -- | Applies a list of events to an aggregate.
  applyEvents :: Listener s e -> Maybe s -> [e] -> Maybe s
  applyEvents = foldl
  
  -- | Delegates a command to an aggregate.
  handle :: Handler s c e -> Listener s e -> Snapshot s e -> c -> Snapshot s e
  handle cmdHandler listener (oldEvents, oldState) cmd = 
    let newEvents = cmdHandler oldState cmd
        newState = applyEvents listener oldState newEvents
    in (oldEvents ++ newEvents, newState) 
