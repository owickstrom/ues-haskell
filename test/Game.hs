module Game where
  import Test.Hspec
  import Test.HUnit
  
  import UES.Core
  import UES.Infrastructure
  import UES.Sample.RPS.Game
  import UES.Sample.RPS.Events
  import UES.Sample.RPS.Commands
  
  type Error = String
  type Story e s = Either Error (Snapshot s e)
    
  given :: [e] -> Story e s
  given evs = Right (evs, Nothing)
  
  when :: Story e s -> Handler s c e -> Listener s e -> c -> Story e s
  when err@(Left _) _ _ _ = err
  when (Right (oldEvs, oldState)) cmdHandler listener cmd =  
    case handle cmdHandler listener (oldEvs, oldState) cmd of
      (evs, state) -> Right (evs, state)
      
  assertHasEvents :: (Eq a, Show a) => Either String (a, t) -> a -> Assertion
  assertHasEvents (Left err) _ = assertFailure err
  assertHasEvents (Right (actual, _)) expected = actual @?= expected
    
  spec :: Spec
  spec =
    let 
      preconditions = given []
      result = when preconditions handleCommand applyEvent (CreateGameCommand "1" "Nils")
      expected = [GameCreatedEvent "Nils"]
    in describe "Rock Paper Scissors" $
      it "should get created if nothing else has happened" $ 
        assertHasEvents result expected
